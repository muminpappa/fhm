# Notebook
These are my notes. Maybe they are helful.

## Set up the virtual environment

    python3 -m venv --system-site-packages venv
    source venv/bin/activate
    python -m pip install -r requirements.txt

## Freeze virtual environment

    pip freeze > requirements.txt

## Set up a database table

Connect to MySQL as root:

    sudo mysql

and create user and database:

    CREATE USER 'kali'@'localhost' IDENTIFIED BY 'passwooord';
    CREATE DATABASE FHM;
    GRANT ALL ON FHM.* TO 'kali'@'localhost';

Continue as normal user 

    mysql -p

and create a table:

    use FHM;
    CREATE TABLE new_infections_per_day (fetch_date DATE, detected_date DATE, number INT);

Allow access from remote:

https://kasunsiyambalapitiya.medium.com/how-to-setup-a-mysql-server-on-local-area-network-lan-c3c5012c7d6b

    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'newpassword' WITH GRANT OPTION;
    FLUSH PRIVILEGES;

and 

    bind-address = 192.168.1.181 

in /etc/mysql/mariadb.conf.d/50-server.cnf, followed by

    sudo systemctl restart mysql

## Jenkins on Kali / Raspi

https://www.jenkins.io/doc/book/installing/linux/ 

Use

    sudo update-alternatives --config java

to activate the correct Java version.

http://kali.lan:8080

Install nginx and configure as reverse proxy.

https://www.jenkins.io/doc/book/system-administration/reverse-proxy-configuration-nginx/ 

Configure nginx for TLS

https://nginx.org/en/docs/http/configuring_https_servers.html 

Update: To get webhooks from Codeberg to work, the certificate must be issued for the correct (public) IP address.
However, the hook complains about the self-signed certificate. So maybe https://letsencrypt.org/getting-started/ 
is the way to go.

See https://linuxconfig.org/generate-ssl-certificates-with-letsencrypt-debian-linux

- Deactivate reverse proxy nginx
- Forward port 80 in router

```
sudo certbot certonly --dry-run --webroot -w /var/www/html/ -d h-144-29.A343.priv.bahnhof.se
sudo certbot certonly --webroot -w /var/www/html/ -d h-144-29.A343.priv.bahnhof.se
```

 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/h-144-29.a343.priv.bahnhof.se/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/h-144-29.a343.priv.bahnhof.se/privkey.pem
   Your cert will expire on 2021-02-23. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.

With new installation (Ubuntu server) I tried the certbot plugin for nginx:

```
sudo apt install python3-certbot-nginx
sudo certbot --nginx
```

## Codeberg integration

Follow https://plugins.jenkins.io/gitea/ and https://mike42.me/blog/2019-05-how-to-integrate-gitea-and-jenkins 
(for the webhook).

## Use Jenkins credentials in Pipelines

Get application password from Wordpress site and create 
credential in Jenkins (username / password).

https://www.jenkins.io/doc/book/pipeline/syntax/#environment

In Jenkinsfile in the stage:
```groovy
environment { 
                WP = credentials('WP_Playground') 
            }
```
This provides the environment variables `WP_USR` and `WP_PSW`
that can be used in the stage, e.g. in the script by
```python
APPLICATIONPASSWORD=os.environ['WP_PSW']
USER=os.environ['WP_USR']
```

## MySQL Python Connector


## Useful links

https://xlrd.readthedocs.io/en/latest/api.html

https://docs.python.org/3/tutorial/index.html

https://dev.mysql.com/doc/refman/5.6/en/loading-tables.html

https://dev.mysql.com/doc/connector-python/en/ 

https://www.atlassian.com/git/tutorials/git-hooks

