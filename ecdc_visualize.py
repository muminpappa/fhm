import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.close('all')
import pandas as pd


# Pick the latest xlsx
fn = 'ecdc.xlsx'
xlsx = pd.ExcelFile(fn)
data = pd.read_excel(fn,sheet_name=xlsx.sheet_names[0])

d = data[data['indicator']=='deaths']

a = d.pivot_table(index='year_week',columns='country_code',values='rate_14_day')

# Anteil der Bevölkerung im Alter von 65 Jahren und darüber [TPS00028]
# https://ec.europa.eu/eurostat/databrowser/bookmark/16e548a9-b9b7-4945-a309-02931535c2c9?lang=de
demo_correction = 21.8/20
a['SWE']=a['SWE']*demo_correction

countries = ['DEU','SWE']
plt.figure(figsize=[8,6])
p=a[countries]['2020-31':].cumsum().plot()
#for tick in p.get_xticklabels():
#    tick.set_rotation(55)
plt.title('Deaths per 1M inhabitants')
plt.grid()
plt.savefig('ecdc.png')

plt.close('all')
plt.figure(figsize=[8,6])
p=a[countries].sort_index(ascending=False).cumsum().sort_index().plot()
plt.title('Deaths per 1M inhabitants')
plt.grid()
plt.savefig('ecdc_backward.png')