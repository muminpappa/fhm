#!/bin/bash
python3 -m venv --system-site-packages venv
source venv/bin/activate
python -m pip install wget bs4 requests pandas matplotlib xlrd
deactivate
